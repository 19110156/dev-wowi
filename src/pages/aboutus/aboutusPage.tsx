import React from "react";
import Banner from "../../components/bannerWowi/banner";
import AboutUs1 from '../../asset/images/aboutus1.png'
import AboutUs2 from '../../asset/images/aboutus2.png'
import Section from "../../components/sectionPor/section";
import Reasons from "../../components/reasons/reasons";
import BannerImg from '../../asset/images/aboutusbanner.png'
import Teamwork from "../../components/teamwork/teamwork";
import ComponentPage from "../../components/componentPage/componentPage";

const AboutusPage=()=>{
    return(
        <ComponentPage isHiddenHeader={false} isHiddenFooter={false}>
                 <Banner image={BannerImg}/>
                 <Section image={AboutUs1} vtImg1={1} vtImg2={2}/>
                 <Section image={AboutUs2} note={"Nguyen Hong Duc"} vtImg1={1} vtImg2={2}/>
                 <Reasons/>
                 <Teamwork/>
        </ComponentPage>
    );
}
export  default  AboutusPage;