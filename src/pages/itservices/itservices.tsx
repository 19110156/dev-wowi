import React from "react";
import style from './style/itservices.module.scss'
import ComponentPage from "../../components/componentPage/componentPage";
import Banner from "../../components/bannerWowi/banner";
import BannerImg from "../../asset/images/itservice-banner.jpg";

import Section from "../../components/sectionPor/section";
import AboutUs1 from "../../asset/images/itservices-section.png";
import ImgITSerVices from "../../asset/images/itservices-img.png";

import Reasons from "../../components/reasons/reasons";

const ItServices=()=>{
    return(
        <ComponentPage isHiddenHeader={false} isHiddenFooter={false}>
            <Banner image={BannerImg}/>

            <Section image={AboutUs1} isHiddenDiscover={false} vtImg1={1} vtImg2={2}/>
            <section className={style.imgItSerVices}>
                <img src={ImgITSerVices} alt={"it-services"}/>
            </section>
            <Reasons/>
        </ComponentPage>


    );
}

export default ItServices;