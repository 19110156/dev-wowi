import React from 'react'
import style from './style/item.module.scss'
import imgBlogItem from '../../../../../asset/images/img-home-blog1.png'
import DiscoverMore from "../../../../../components/discovermore/discovermore";
const ItemBlog=()=>{
    return(
        <div className={style.item}>
            <img src={imgBlogItem} alt={"img-hom-blog"}/>
            <p className={style.item_blog_title}> CDN account to help you manage your icons even more easily.</p>
            <p className={style.item_blog_border}></p>
            <p className={style.item_blog_content}> CDN account to help you manage your icons even more easily.</p>
            <p className={style.item_blog_discoverMore}><DiscoverMore/></p>
        </div>
    );
}
export default ItemBlog;