import React from "react";
import style from './style/blog.module.scss'
import ItemBlog from "./itemblog/itemblog";
const Blog=()=>{
    return(
        <section>
            <div className={style.blog_head}>
                <p className={style.blog_head_title}>BLOG</p>
                <p className={style.blog_head_note}>Font Awesome CDN can load your icons in the background, so that your site's content and navigation load as quickly as possible for your users. Configure, Edit, and Update Embed Codes</p>
            </div>
            <div className={style.blog_item}>
                <ItemBlog/>
                <ItemBlog/>
            </div>
    </section>
    );
}

export default Blog;