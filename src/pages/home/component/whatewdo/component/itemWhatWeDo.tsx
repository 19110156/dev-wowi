import React from "react";
import style from './style/ItemWhatWeDo.module.scss'
import ItemWhatWeDoImg from '../../../../../asset/images/Item-What-We-Do.png'
const ItemWhatWeDo=()=>{
    return(
        <div className={style.item}>
            <img src={ItemWhatWeDoImg} alt={"img item whatwedo"}/>
            <p>Trusted It Advisor </p>
            <p>Control IT Costs </p>
        </div>
    )
}
export default ItemWhatWeDo