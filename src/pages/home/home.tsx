import React from "react";
import ComponentPage from "../../components/componentPage/componentPage";
import Banner from "../../components/bannerWowi/banner";
import BannerImg from "../../asset/images/bannerhome.png";
import Section from "../../components/sectionPor/section";
import AboutUs1 from "../../asset/images/home-section-aboutus.png";
import AboutUs2 from "../../asset/images/home-section-itservices.png";
import ImgIntroduction from "../../asset/images/home-section-introduction.png";
import Reasons from "../../components/reasons/reasons";
import Whatwedo from "./component/whatewdo/whatwedo";
import Blog from "./component/blog/Blog";
const Home=()=>{
    return(
        <ComponentPage isHiddenHeader={false} isHiddenFooter={false}>
            <Banner image={BannerImg}/>
            <Whatwedo/>
            <Section image={AboutUs1} isHiddenDiscover={true} vtImg1={1} vtImg2={2}/>
            <Section image={AboutUs2} note={"Nguyen Hong Duc"} isHiddenDiscover={true} vtImg1={2} vtImg2={1}/>
            <Section image={ImgIntroduction} note={"Nguyen Hong Duc"} isHiddenDiscover={true} vtImg1={1} vtImg2={2}/>
            <Blog/>
            <Reasons/>
        </ComponentPage>
    );
}
export default  Home;