import React from "react";
import style from './style/itServicesItem.module.scss'
import  itservice from '../../asset/images/itservice1.png'
const ItServicesItem=()=>{
    return(
        <div className={style.itservicesitem}>
                <div className={style.images}>
                    <img src={itservice} alt={'imageItServices'}/>
                </div>
                <div className={style.action}>
                    <span> Manged IT Services</span>
                    <p>Discover More <i className="fa-solid fa-angles-right"></i></p>
                </div>
        </div>
    );
}
export default  ItServicesItem;