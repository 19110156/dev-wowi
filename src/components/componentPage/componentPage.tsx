import React from "react";
import Header from "../header/header";
import Footer from "../footer/footer";
import HeaderHAVU from "../header/header";

interface IHidden{
    isHiddenHeader:boolean
    isHiddenFooter:boolean
}
  function  ComponentPage(props:any){
      return(
          <>
              {
                  !props?.isHiddenHeader &&<HeaderHAVU/>
              }
              {
                  props?.children
              }
              {
                  !props?.isHiddenFooter &&<Footer/>
              }
          </>

      )

}

export default ComponentPage;