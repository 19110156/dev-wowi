import React from "react";
import style from './style/header.module.scss'
import logo  from '../../asset/images/logo.png'
const HeaderHAVU=()=>{
    return(


           <div className={style.header}>
               <div className={style.logo}>
                   <img src={logo} alt={'logo'}/>
               </div>
               <div className={style.listmenu}>
                   <ul>
                       <li>Home</li>
                       <li>About US</li>
                       <li>IT Services</li>
                       <li>Show Case</li>
                       <li>Blog</li>
                       <li>Contact</li>
                   </ul>

               </div>
           </div>

    );
}

export  default  HeaderHAVU
