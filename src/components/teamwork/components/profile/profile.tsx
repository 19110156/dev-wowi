
import React from 'react'
import style from './style/style.module.scss'
import imgTeamwork from '../../../../asset/images/about-teamwork.png'

const Profile=()=>{
    return(
        <div className={style.profile}>
            <img src={imgTeamwork} alt={"img profile"}/>
            <div className={style.profile_info}>
                <p className={style.title}>Mark Potter</p>
                <p className={style.duty}>President & CEO</p>
            </div>
        </div>
    )
}
export default Profile;