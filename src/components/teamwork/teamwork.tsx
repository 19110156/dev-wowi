import React from "react";
import style from "./style/teamwork.module.scss";
import Profile from "./components/profile/profile";



const Teamwork=()=>{
    return(
        <section className={style.teamwork}>
            <div className={style.teamwork_title}>
                <p className={style.title__teamwork}>TEAM WORK</p>
                <p className={style.content__teamwork}>Leader team has a long history of success in technology ,bussiness  management and franchising</p>
                <div className={style.button__teamwork}>
                    <a href={""}>Our Leadership</a>
                    <a  href={""}>Our Developer</a>
                </div>
            </div>
            <div className={style.teamwork_profile}>

                <Profile/>
                <Profile/>
                <Profile/>
                <Profile/>
            </div>

        </section>
    );
}

export default Teamwork;