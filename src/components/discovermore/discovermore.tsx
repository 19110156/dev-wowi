import React  from "react";
import style from './style/discovermore.module.scss'
const DiscoverMore=()=>{
    return(
        <div className={style.discoverMore}>
            <span>Discover More  </span>
                <div className={style.icons}>
                   <span className="material-icons">
                    navigate_next
                </span>
                <span className="material-icons next">
                     arrow_forward_ios
                </span>
            </div>

        </div>
    )
}
export  default DiscoverMore