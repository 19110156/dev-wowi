import React, {Fragment} from "react";
import style from './style/section.module.scss'
import DiscoverMore from "../discovermore/discovermore";
import style2 from './style/section2.module.scss'
interface variables{
    image:any
    note?:string
    isHiddenDiscover?:Boolean
    vtImg1?:number
    vtImg2?:number
}
const Section=({image,note,isHiddenDiscover,vtImg1,vtImg2}:variables)=>{



    return(
        <section className={vtImg1===1?style.section:style2.section}>
            <div className={vtImg1===1?style.section__left:style2.section__right_content } style={{order:vtImg1}}>
                <p className={vtImg1===1?style.title:style2.title}>YOUR TRUSTED SOURCE IN IT SERVICES & SUPPORT</p>
                <p className={vtImg1===1?style.note:style2.note}>{note?note:""}</p>
                <p className={vtImg1===1?style.content:style2.content}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                <p  className={vtImg1===1?style.discover:style2.discover}>
                    {isHiddenDiscover?<DiscoverMore/>:<></>}
                </p>
            </div>
            <div className={vtImg1===1?style.section__right:style2.section__left_img} style={{order:vtImg2}}  >
                <img src={image} alt={"Img section"}/>
            </div>
         </section>
    );
}
export default  Section;