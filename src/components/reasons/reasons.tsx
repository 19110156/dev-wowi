import React from "react";
import ReasonsItem from "./component/reasonsItems/reasonsItem";
import style from './style/reason.module.scss'
const Reasons =()=>{
    return (
        <section className={style.reasons}>
            <div className={style.reason_title}>
                <p className={style.title__reason}>5 REASONS TO COOPERATE WITH US</p>
                <p className={style.title__choose}>Why choose we</p>
            </div>
            <div className={style.reason_item}>
                <ReasonsItem/>
                <ReasonsItem/>
                <ReasonsItem/>
            </div>
        </section>
    );
}

export default Reasons;