import React from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import AboutusPage from "./pages/aboutus/aboutusPage";
import Home from "./pages/home/home";
import ItServices from "./pages/itservices/itservices";
function App() {

  return (
      <Router>
          <div className="App">

              <Routes>
                  <Route path={'/home'} element={< Home />}/>
                  <Route  path={'/about'} element={< AboutusPage />}/>
                  <Route  path={'/it-services'} element={< ItServices />}/>

              </Routes>
          </div>
      </Router>

  );
}

export default App;
