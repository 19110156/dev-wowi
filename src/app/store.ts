import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';

import revertationReducer from '../features/revertationSlice'
export const store = configureStore({
  reducer: {
      revertation:revertationReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
